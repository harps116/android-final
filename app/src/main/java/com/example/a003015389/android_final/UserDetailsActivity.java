package com.example.a003015389.android_final;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.a003015389.android_final.dataAccess.UserDataAccess;
import com.example.a003015389.android_final.models.User;

/**
 * Created by 003015389 on 11/22/2017.
 */

public class UserDetailsActivity extends AppCompatActivity {

    public static final String USER_ID_EXTRA = "userid";

    SQLiteUserHelper dbHelper;
    UserDataAccess da;
    AppClass app;
    User user;
    EditText txtFirstName;
    EditText txtEmail;
    RadioGroup rgFavoriteMusic;
    CheckBox chkActive;
    Button btnSave;

    Button btnClearForm;
    Button btnDeleteData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);

        dbHelper = new SQLiteUserHelper(this);
        da = new UserDataAccess(dbHelper);
        app = (AppClass)getApplication();


        txtFirstName = (EditText)findViewById(R.id.txtFirstName);
        txtEmail = (EditText)findViewById(R.id.txtEmail);
        rgFavoriteMusic = (RadioGroup)findViewById(R.id.rgFavoriteMusic);
        chkActive = (CheckBox)findViewById(R.id.chkActive);
        btnSave = (Button)findViewById(R.id.btnSave);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDataFromUI();
                if( user != null ){
                    if( user.getId() > 0 ){
                        da.updateUser(user);
                        Intent i = new Intent(UserDetailsActivity.this, MainActivity.class);
                        startActivity(i);
                    }else{
                        da.insertUser(user);
                        Intent i = new Intent(UserDetailsActivity.this, MainActivity.class);
                        startActivity(i);
                    }
                }else{
                    Log.d("No", "user");
                }
            }
        });

        btnClearForm = (Button)findViewById(R.id.btnClearForm);
        btnClearForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetForm();
            }
        });

        btnDeleteData = (Button)findViewById(R.id.btnDeleteData);
        btnDeleteData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(user.getId() > 0){
                    AlertDialog.Builder alert = new AlertDialog.Builder(
                            UserDetailsActivity.this);
                    alert.setTitle("Confirm!");
                    alert.setMessage("Are you sure to delete this user?");
                    alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            da.deleteItem(user);
                            Toast.makeText(UserDetailsActivity.this, user.getFirstName() + " Deleted!", Toast.LENGTH_LONG).show();
                            Intent i = new Intent(UserDetailsActivity.this, MainActivity.class);
                            startActivity(i);
                            dialog.dismiss();
                        }
                    });
                    alert.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent i = new Intent(UserDetailsActivity.this, MainActivity.class);
                            startActivity(i);
                            dialog.dismiss();
                        }
                    });

                    alert.show();
                }
            }
        });

        Intent i = getIntent();
        long userId = i.getLongExtra(USER_ID_EXTRA,-1);
        if(userId >= 0){
            user = app.getUserById(userId);
            putDataInUI();
        }
    }

    private void getDataFromUI(){

        String firstName = txtFirstName.getText().toString();
        String email = txtEmail.getText().toString();
        boolean active = chkActive.isChecked();
        // Here's how we can set the favorite music...
        User.Music favoriteMusic = null;
        int selectedRadioButtonId = rgFavoriteMusic.getCheckedRadioButtonId();

        switch(selectedRadioButtonId){
            case R.id.rbCountry:
                favoriteMusic = User.Music.COUNTRY;
                break;
            case R.id.rbRap:
                favoriteMusic = User.Music.RAP;
                break;
            case R.id.rbJazz:
                favoriteMusic = User.Music.JAZZ;
                break;
        }

        if(TextUtils.isEmpty(firstName) && TextUtils.isEmpty(email) && favoriteMusic == null ){

            Toast.makeText(this, "Please fill in fields", Toast.LENGTH_LONG).show();

        }else if( TextUtils.isEmpty(firstName) ) {

            Toast.makeText(this, "Please enter a first name", Toast.LENGTH_LONG).show();

        }else if( TextUtils.isEmpty(email) ){

            Toast.makeText(this, "Please enter an email", Toast.LENGTH_LONG).show();

        }else if( favoriteMusic == null ){

            Toast.makeText(this, "Please enter a favorite music", Toast.LENGTH_LONG).show();
        }
        else{
            if(user == null){
                user = new User();
                user.setFirstName(firstName);
                user.setEmail(email);
                user.setFavoriteMusic(favoriteMusic);
                user.setActive(active);
            }else {
                user = new User(user.getId(), firstName, email, favoriteMusic, active);
            }

            Toast.makeText(this, "Data successfully saved! :)", Toast.LENGTH_LONG).show();
        }

    }

    private void putDataInUI(){

        txtFirstName.setText(user.getFirstName());
        txtEmail.setText(user.getEmail());
        chkActive.setChecked(user.isActive());

        switch(user.getFavoriteMusic()){
            case COUNTRY:
                rgFavoriteMusic.check(R.id.rbCountry);
                break;
            case JAZZ:
                rgFavoriteMusic.check(R.id.rbJazz);
                break;
            case RAP:
                rgFavoriteMusic.check(R.id.rbRap);
                break;
        }
    }

    private void resetForm(){
        rgFavoriteMusic.clearCheck();
        txtFirstName.setText("");
        txtEmail.setText("");
        chkActive.setChecked(false);
    }
}
