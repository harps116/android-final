package com.example.a003015389.android_final;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.menu.MenuAdapter;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import com.example.a003015389.android_final.dataAccess.UserDataAccess;
import com.example.a003015389.android_final.models.User;

public class MainActivity extends AppCompatActivity {

    SQLiteUserHelper dbHelper;
    UserDataAccess da;


    AppClass app;
    ListView userListView;
    Button addUserData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbHelper = new SQLiteUserHelper(this);
        da = new UserDataAccess(dbHelper);

        app.users = da.getAllUsers();

        addUserData = (Button)findViewById(R.id.btnAddUser);

        addUserData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, UserDetailsActivity.class);
                startActivity(i);
            }
        });
        app = (AppClass)getApplication();

        userListView = (ListView)findViewById(R.id.userListView);

        ArrayAdapter adapter = new ArrayAdapter<User>(this, R.layout.custom_user_list_item, R.id.lblFirstName, app.users){

            @Override
            public View getView(int position, View convertView, ViewGroup parent){
                View listItemView = super.getView(position, convertView, parent);
                User currentUser = app.users.get(position);
                listItemView.setTag(currentUser);

                TextView lbl = (TextView)listItemView.findViewById(R.id.lblFirstName);
                CheckBox chk = (CheckBox)listItemView.findViewById(R.id.chkActive);

                lbl.setText(currentUser.getFirstName());
                chk.setChecked(currentUser.isActive());
                chk.setText("Active");
                chk.setTag(currentUser);

                chk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        User selectedUser = (User)view.getTag();
                        CheckBox c = (CheckBox)view;
                        selectedUser.setActive(c.isChecked());
                    }
                });

                listItemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        User selectedUser = (User)view.getTag();
                        Intent i = new Intent(MainActivity.this, UserDetailsActivity.class);
                        i.putExtra(UserDetailsActivity.USER_ID_EXTRA, selectedUser.getId());
                        startActivity(i);
                    }
                });

                return listItemView;

            }

        };

        userListView.setAdapter(adapter);

    }
}
