package com.example.a003015389.android_final;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.a003015389.android_final.models.User;

import java.util.ArrayList;

/**
 * Created by 003015389 on 11/22/2017.
 */

public class UserListAdapter extends ArrayAdapter {

    ArrayList<User> users;

    public UserListAdapter(Context context, int layoutResourceID, int x, ArrayList<User> users){
        super(context, layoutResourceID, x, users);
        this.users = users;

    }

    @Override
    public View getView(int position, View view, ViewGroup parent){
        View itemView = super.getView(position, view, parent);
        TextView lblFirstName = (TextView)itemView.findViewById(R.id.lblFirstName);
        CheckBox chkActive = (CheckBox)itemView.findViewById(R.id.chkActive);

        User u = users.get(position);
        lblFirstName.setText(u.getFirstName());
        chkActive.setChecked(u.isActive());

        // TODO:
        // 1. chkActive.setTag(u)
        // 2. hook up the listener on the checkbox (and use getTag() in the callback)

        return itemView;
    }

}