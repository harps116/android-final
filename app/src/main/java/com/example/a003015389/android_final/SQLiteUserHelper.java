package com.example.a003015389.android_final;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.example.a003015389.android_final.dataAccess.UserDataAccess;

/**
 * Created by 003015389 on 11/20/2017.
 */

public class SQLiteUserHelper extends SQLiteOpenHelper {

    private static final String TAG = "SQLiteUserHelper";
    private static final String DATA_BASE_NAME = "users.db";
    private static final int DATABASE_VERSION = 1;

    public SQLiteUserHelper(Context context) {
        super(context, DATA_BASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = UserDataAccess.TABLE_CREATE;
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

}
