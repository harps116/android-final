package com.example.a003015389.android_final.dataAccess;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.a003015389.android_final.SQLiteUserHelper;
import com.example.a003015389.android_final.models.User;

import java.util.ArrayList;

/**
 * Created by 003015389 on 11/20/2017.
 */

public class UserDataAccess {

    public static final String TAG = "UserDataAccess";

    SQLiteUserHelper dbHelper;
    SQLiteDatabase db;

    public static final String  TABLE_NAME = "users";
    public static final String  COLUMN_USER_ID = "_id";
    public static final String  COLUMN_USER_FIRST_NAME = "first_name";
    public static final String  COLUMN_FAVEORITE_MUSIC = "faveorite_music";
    public static final String  COLUMN_USER_EMAIL = "email";
    public static final String  COLUMN_ACTIVE = "active";

    public static final String TABLE_CREATE = String.format("create table %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT, %s TEXT, %s TEXT, %s INTEGER)",
            TABLE_NAME,
            COLUMN_USER_ID,
            COLUMN_USER_FIRST_NAME,
            COLUMN_FAVEORITE_MUSIC,
            COLUMN_USER_EMAIL,
            COLUMN_ACTIVE);

    public UserDataAccess(SQLiteUserHelper dbHelper){
        this.dbHelper = dbHelper;
        this.db = this.dbHelper.getWritableDatabase();
    }

    public User insertUser(User i){
        ContentValues values = new ContentValues();
        values.put(COLUMN_USER_FIRST_NAME, i.getFirstName());
        values.put(COLUMN_USER_EMAIL, i.getEmail());
        values.put(COLUMN_FAVEORITE_MUSIC, i.getFavoriteMusic().toString());
        values.put(COLUMN_ACTIVE, i.isActive());
        long insertId = db.insert(TABLE_NAME, null, values);
        i.setId(insertId);
        return i;
    }

    public ArrayList<User> getAllUsers(){
        ArrayList<User> users = new ArrayList<User>();
        String query = String.format("SELECT %s, %s, %s, %s, %s FROM %s", COLUMN_USER_ID, COLUMN_USER_FIRST_NAME, COLUMN_USER_EMAIL,COLUMN_FAVEORITE_MUSIC,COLUMN_ACTIVE, TABLE_NAME);
        Cursor c = db.rawQuery(query, null);
        c.moveToFirst();
        while(!c.isAfterLast()){
            User.Music um = null;
            String str = c.getString(3);
            if(str.equals("JAZZ")){
                um = User.Music.JAZZ;
            }else if (str.equals("RAP")){
                um = User.Music.RAP;
            }else{
                um = User.Music.COUNTRY;
            }
            User u = new User();
            u.setId(c.getLong(0));
            u.setFirstName(c.getString(1));
            u.setEmail(c.getString(2));
            u.setFavoriteMusic(um);
            u.setActive(c.getInt(4) != 0);

            users.add(u);
//            Log.d("FOO", u.toString());
            c.moveToNext();
        }
        c.close();
        return users;
    }

    public User updateUser(User u){
        ContentValues values = new ContentValues();
        values.put(COLUMN_USER_ID, u.getId());
        values.put(COLUMN_USER_FIRST_NAME, u.getFirstName());
        values.put(COLUMN_USER_EMAIL, u.getEmail());
        values.put(COLUMN_FAVEORITE_MUSIC, u.getFavoriteMusic().toString());
        Log.d("USER", u.toString());
        int rowsUpdated = db.update(TABLE_NAME, values, "_id = " + u.getId(), null);
        return u;
    }

    public int deleteItem(User u){
        int rowsDeleted = db.delete(TABLE_NAME, COLUMN_USER_ID + " = " + u.getId(), null);
        return rowsDeleted;
    }
}
